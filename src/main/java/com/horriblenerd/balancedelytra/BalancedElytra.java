package com.horriblenerd.balancedelytra;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ElytraItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.UUID;

import static com.horriblenerd.balancedelytra.Registration.SUPER_JUMP;

@Mod(BalancedElytra.MODID)
public class BalancedElytra {
    public static final String MODID = "balancedelytra";
    public static final UUID uuid = UUID.fromString("eda2702c-afd7-447b-be55-0bb5e6b5abc6");
    private static final Logger LOGGER = LogManager.getLogger();
    public int ELYTRA_FLIGHT_TIME = 5000;

    public BalancedElytra() {
        LOGGER.debug("Init");
        Registration.init();
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void onUseItem(PlayerInteractEvent.RightClickItem event) {

        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity playerEntity = (PlayerEntity) event.getEntityLiving();
            Item item = event.getEntityLiving().getItemInHand(event.getHand()).getItem();
            if (item == Items.FIREWORK_ROCKET) {
                if (playerEntity.isFallFlying()) {
                    event.setCanceled(true);
                }
            }
        }
    }

    @SubscribeEvent
    // TODO: weather?
    // TODO: updrafts
    public void onLivingUpdate(LivingEvent.LivingUpdateEvent event) {
        LivingEntity entityLiving = event.getEntityLiving();
        if (entityLiving instanceof PlayerEntity) {
            if (entityLiving.isFallFlying()) {
                World world = entityLiving.getCommandSenderWorld();
                if (world.getGameTime() % 2 == 0) {
                    BlockPos pos = new BlockPos(entityLiving.getX(), entityLiving.getY(), entityLiving.getZ());
                    boolean isRaining = world.isRainingAt(pos);
                    Biome biome = world.getBiome(pos);
                    Biome.RainType rainType = biome.getPrecipitation();
                    float temperature = biome.getTemperature(pos);
                    boolean highHumidity = biome.isHumid();

                    Vector3d motion = entityLiving.getDeltaMovement();
                    LOGGER.debug("X: " + Math.abs(motion.x()));
                    LOGGER.debug("Z: " + Math.abs(motion.z()));
                    LOGGER.debug("TOTAL: " + (Math.abs(motion.x()) + Math.abs(motion.z())));
                    if (entityLiving.isShiftKeyDown()) {
                        LOGGER.debug("SLOWING DOWN");
                        entityLiving.setDeltaMovement(motion.multiply(0.90D, 0.95D, 0.90D));
                    }
                    else if (entityLiving.isSprinting()) {
                        if (Math.abs(motion.x()) + Math.abs(motion.z()) < 4) {
                            LOGGER.debug("SPEEDING UP");
                            entityLiving.setDeltaMovement(motion.multiply(1.02D, 1D, 1.02D));
                        }
                        else {
                            LOGGER.debug("TOO FAST");
                        }
                    }

                }
            }
        }
    }

    @SubscribeEvent
    public void onJump(LivingEvent.LivingJumpEvent event) {
        LivingEntity entityLiving = event.getEntityLiving();
        if (entityLiving instanceof PlayerEntity) {
            PlayerEntity playerEntity = (PlayerEntity) entityLiving;
            ItemStack stack = playerEntity.getItemBySlot(EquipmentSlotType.CHEST);
            if (playerEntity.isShiftKeyDown() && stack.getItem() instanceof ElytraItem && stack.getDamageValue() + 1 < stack.getMaxDamage()) {
                int enchantmentLevel = EnchantmentHelper.getItemEnchantmentLevel(SUPER_JUMP.get(), stack);
                if (enchantmentLevel <= 0) {
                    return;
                }

                LOGGER.debug("JUMP");
                Vector3d motion = playerEntity.getDeltaMovement();
                playerEntity.setDeltaMovement(motion.x, motion.y * 2.3 * enchantmentLevel, motion.z);
                Random rand = playerEntity.level.random;

                playerEntity.level.playSound(playerEntity, playerEntity, SoundEvents.EVOKER_CAST_SPELL, SoundCategory.PLAYERS, 1.0F, 1.0F);
                for (double y = playerEntity.getY(); y < playerEntity.getY() + 14 * enchantmentLevel; y += 0.5D) {
                    playerEntity.level.addParticle(ParticleTypes.POOF, playerEntity.getX() + rand.nextGaussian() * 2D, y - 0.3D + rand.nextGaussian(), playerEntity.getZ() + rand.nextGaussian() * 2D, rand.nextGaussian() * 0.05D, -motion.y * 0.25D, rand.nextGaussian() * 0.05D);
                    playerEntity.level.addParticle(ParticleTypes.FIREWORK, playerEntity.getX() + rand.nextGaussian() * 2D, y - 0.3D + rand.nextGaussian(), playerEntity.getZ() + rand.nextGaussian() * 2D, rand.nextGaussian() * 0.05D, -motion.y * 0.25D, rand.nextGaussian() * 0.05D);
                    playerEntity.level.addParticle(ParticleTypes.EFFECT, playerEntity.getX() + rand.nextGaussian() * 2D, y - 0.3D + rand.nextGaussian(), playerEntity.getZ() + rand.nextGaussian() * 2D, rand.nextGaussian() * 0.05D, -motion.y * 0.25D, rand.nextGaussian() * 0.05D);
                }
            }
        }
    }

    @SubscribeEvent
    public void onDamage(LivingDamageEvent event) {
        if (event.getSource() == DamageSource.FLY_INTO_WALL) {
            if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity playerEntity = (PlayerEntity) event.getEntityLiving();
                event.setAmount(event.getAmount() * 1 - (0.12f * 4));
            }
        }
    }


}

package com.horriblenerd.balancedelytra;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.inventory.EquipmentSlotType;

import static com.horriblenerd.balancedelytra.Registration.ELYTRA_TYPE;

/**
 * Created by HorribleNerd on 27/02/2021
 */
public class SuperJumpEnchantment extends Enchantment {

    protected SuperJumpEnchantment() {
        super(Rarity.RARE, ELYTRA_TYPE, new EquipmentSlotType[]{EquipmentSlotType.CHEST});
    }

    @Override
    public boolean isTreasureOnly() {
        return false;
    }

    @Override
    public boolean isAllowedOnBooks() {
        return true;
    }

    @Override
    public int getMinLevel() {
        return 1;
    }

    @Override
    public int getMaxLevel() {
        return 3;
    }


}
